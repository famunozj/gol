package es.friki.gol.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.friki.gol.business.CelulaService;
import es.friki.gol.business.EntornoService;
import es.friki.gol.dto.CelulaDto;
import es.friki.gol.dto.EntornoDto;
import es.friki.gol.persistence.entity.Celula;
import es.friki.gol.persistence.entity.Entorno;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

@RestController
@Scope("session")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/gol")
@Tag(name = "gol")
@Slf4j
public class GolController {

	@Autowired
	@Qualifier("celulaService")
	private CelulaService celulaService;

	@Autowired
	@Qualifier("entornoService")
	private EntornoService entornoService;

	@Autowired
	ObjectFactory<HttpSession> httpSessionFactory;

	private Celula celula;

	private Entorno entorno;

	private static String SESSION_LOG = "Session: ";


	@GetMapping("/getTableroInicial/{ancho}/{alto}")
	public EntornoDto getTableroInicial(@PathVariable int ancho, @PathVariable int alto) {	
		HttpSession session = httpSessionFactory.getObject();	
		log.info(SESSION_LOG + session.getId());			
		entorno = entornoService.crearEntornoInicial(ancho, alto);
		return entornoService.getEntornoDto(entorno);
	}

	@GetMapping("/getSiguientePaso")
	public EntornoDto getSiguientePaso() {	
		HttpSession session = httpSessionFactory.getObject();	
		log.info(SESSION_LOG + session.getId());			
		if (entorno == null) {
			entorno = entornoService.crearEntornoInicial(2, 2);					
		}			
		
		return entornoService.getEntornoDto(entornoService.getSiguientePaso(entorno));
	}

	@GetMapping("/getCelulaDto")
	public CelulaDto getCelulaDto() {	
		HttpSession session = httpSessionFactory.getObject();	
		log.info(SESSION_LOG + session.getId());	
		return celulaService.getCelulaDto();
	}
	

	@GetMapping("/siguienteEstado")
	public CelulaDto getSiguienteEstado() {		
		HttpSession session = httpSessionFactory.getObject();		
		log.info(SESSION_LOG + session.getId());	

		if (celula == null) {
			celula = celulaService.getCelula();
		} 

		return celulaService.getSiguienteEstado(celula);
	}

	@GetMapping("/")
    public String helloAdmin() {
        return "hello admin";
    }
}


