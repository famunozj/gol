package es.friki.gol.estado;

public enum Estado {
    VIVO ("Vivo") {
        @Override
        public Estado matar() {                        
            return Estado.MUERTO;
        }
        @Override
        public Estado permanecerVivo() {            
            return this;
        }

    },	
	MUERTO ("Muerto"){
        @Override
        public Estado nacer() {            
            return Estado.VIVO;
        }
        @Override
        public Estado permanecerMuerto() {            
            return this;
        }

    };

    private String texto;
	
	private Estado(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

    public Estado matar() {
        return this;
    }

    public Estado nacer() {
        return this;
    }

    public Estado permanecerVivo() {
        return this;
    }

    public Estado permanecerMuerto() {
        return this;
    }
}
