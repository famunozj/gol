package es.friki.gol.business;

import es.friki.gol.dto.CelulaDto;
import es.friki.gol.persistence.entity.Celula;

public interface CelulaService {

	Celula getCelula(); 
	CelulaDto getCelulaDto(); 
	CelulaDto getSiguienteEstado(Celula celula); 
}
