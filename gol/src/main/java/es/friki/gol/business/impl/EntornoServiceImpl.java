package es.friki.gol.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import es.friki.gol.business.EntornoService;
import es.friki.gol.conversor.EntornoConv;
import es.friki.gol.dto.EntornoDto;
import es.friki.gol.persistence.EntornoRepository;
import es.friki.gol.persistence.entity.Entorno;
import es.friki.gol.util.UtilTablero;

@Service("entornoService")
public class EntornoServiceImpl implements EntornoService {	

	@Autowired
	@Qualifier("entornoConv")
	private EntornoConv entornoConv;

	@Autowired
	@Qualifier("utilTablero")
	private UtilTablero utilTablero;

	@Autowired
	@Qualifier("entornoRepository")
	private EntornoRepository entornoRepository;

	@Override
	public Entorno crearEntornoInicial(int ancho, int alto) {			

		return entornoRepository.crearInicial(ancho, alto);
	}
	

	@Override
	public EntornoDto getEntornoDto(Entorno entorno) {
		
		return entornoConv.toDto(entorno);
	}

	@Override
	public Entorno getSiguientePaso(Entorno entorno) {
		
		return utilTablero.getSiguientePaso(entorno);
	}

}
