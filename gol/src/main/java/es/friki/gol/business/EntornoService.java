package es.friki.gol.business;

import es.friki.gol.dto.EntornoDto;
import es.friki.gol.persistence.entity.Entorno;

public interface EntornoService {

	Entorno crearEntornoInicial(int ancho, int alto); 	
	EntornoDto getEntornoDto(Entorno entorno); 		
	Entorno getSiguientePaso(Entorno entorno);
	
}
