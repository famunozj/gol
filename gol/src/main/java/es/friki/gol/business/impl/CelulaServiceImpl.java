package es.friki.gol.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import es.friki.gol.business.CelulaService;
import es.friki.gol.conversor.CelulaConv;
import es.friki.gol.dto.CelulaDto;
import es.friki.gol.estado.Estado;
import es.friki.gol.persistence.entity.Celula;

@Service("celulaService")
public class CelulaServiceImpl implements CelulaService {	

	@Autowired
	@Qualifier("celulaConv")
	private CelulaConv celulaConv;

	@Override
	public CelulaDto getCelulaDto() {
		Celula model = new Celula();
		return celulaConv.toDto(model);
	}

	@Override
	public CelulaDto getSiguienteEstado(Celula celula) {

		if (celula.getEstado() == Estado.VIVO) {
            celula.matar();
        } else {
            celula.nacer();
        }
		return celulaConv.toDto(celula);
	}

	@Override
	public Celula getCelula() {	
		return new Celula();		
	}

}
