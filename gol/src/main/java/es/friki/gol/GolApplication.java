package es.friki.gol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class GolApplication {

	public static void main(String[] args) {
		SpringApplication.run(GolApplication.class, args);
	}

}
