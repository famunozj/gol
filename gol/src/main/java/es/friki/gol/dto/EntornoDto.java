package es.friki.gol.dto;

import java.io.Serializable;
import lombok.Data;


@Data
public class EntornoDto implements Serializable {

	private String[][] entorno;

	private int alto;

    private int ancho;

	public EntornoDto(int ancho, int alto) {
		this.setAlto(alto);
		this.setAncho(ancho);
		this.entorno = new String[ancho][alto];		
	}

	public void setCelula(int x, int y, String estado) {
		entorno[x][y] = estado;
	}
	
	
}
