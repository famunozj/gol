package es.friki.gol.util;

import org.springframework.stereotype.Component;

import es.friki.gol.estado.Estado;
import es.friki.gol.persistence.entity.Celula;
import es.friki.gol.persistence.entity.Entorno;
import lombok.Data;

@Component("utilTablero")
@Data
public class UtilTablero {

	public Entorno getSiguientePaso(Entorno entorno) {
		int ancho = entorno.getAncho();
		int alto = entorno.getAlto();

		Entorno tablero = new Entorno(ancho, alto);
		Celula celula;
		for (int x = 0; x < ancho; x++) {
			for (int y = 0; y < alto; y++) {				
				celula = entorno.getCelula(x, y);
				if (celula.getEstado() == Estado.VIVO) {										
					celula.matar();										
				} else {
					celula.nacer();
				}				
				tablero.setCelula(x, y, celula);
			}
		}	

		return tablero;
	}

}
