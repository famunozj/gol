package es.friki.gol.persistence.entity;

import lombok.Data;

@Data
public class Entorno {

    private Celula[][] celulas;

    private int alto;

    private int ancho;
	
	public Entorno(int ancho, int alto) {
        this.setAncho(ancho);
        this.setAlto(alto);
		celulas = new Celula[ancho][alto];
	}
	
	public Celula[][] getCeldas() {
		return celulas;
	}

	public void setCelula(int x, int y, Celula celula) {        
        celulas[x][y] = celula;        
	}

    public Celula getCelula(int x, int y) {
		return celulas[x][y];
	}

}

