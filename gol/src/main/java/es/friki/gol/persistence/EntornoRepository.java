package es.friki.gol.persistence;

import es.friki.gol.persistence.entity.Entorno;

public interface EntornoRepository {
    Entorno getEntorno();
    Entorno crearInicial(int ancho, int alto);
}


