package es.friki.gol.persistence.entity;

import java.io.Serializable;

import es.friki.gol.estado.Estado;
import lombok.Data;

@Data
public class Celula implements Serializable{

   private Estado estado;

   public Celula() {
      this.estado = Estado.MUERTO;
   }

   public void nacer() {
      estado = estado.nacer();
   }

   public void matar() {
      estado = estado.matar();
   }

   public void permanecerVivo() {
      estado = estado.permanecerVivo();
   }

   public void permanecerMuerto() {
      estado = estado.permanecerMuerto();
   }
}
