package es.friki.gol.persistence.impl;

import org.springframework.stereotype.Repository;

import es.friki.gol.persistence.EntornoRepository;
import es.friki.gol.persistence.entity.Celula;
import es.friki.gol.persistence.entity.Entorno;


@Repository("entornoRepository")
public class EntornoRepositoryImpl implements EntornoRepository {

    private Entorno entorno;

    @Override
    public Entorno getEntorno() {
        
        return entorno;
    }
    
    @Override
    public Entorno crearInicial(int ancho, int alto) {
		entorno = new Entorno(ancho, alto);
		Celula celula;
		for (int x = 0; x < ancho; x++) {
			for (int y = 0; y < alto; y++) {
				celula = new Celula();
				entorno.setCelula(x, y, celula);				
			}
		}	
		
		return entorno;
	}
    
}
