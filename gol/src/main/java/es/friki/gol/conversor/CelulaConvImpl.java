package es.friki.gol.conversor;

import org.springframework.stereotype.Component;

import es.friki.gol.dto.CelulaDto;
import es.friki.gol.persistence.entity.Celula;

@Component("celulaConv")
public class CelulaConvImpl implements CelulaConv {	
	
	@Override
	public CelulaDto toDto(Celula model) {
		CelulaDto dto = new CelulaDto();
		dto.setEstado(model.getEstado().getTexto());
		return dto;
	}

}
