package es.friki.gol.conversor;

import es.friki.gol.dto.EntornoDto;
import es.friki.gol.persistence.entity.Entorno;

public interface EntornoConv {
	
	public EntornoDto toDto(Entorno model);

}
