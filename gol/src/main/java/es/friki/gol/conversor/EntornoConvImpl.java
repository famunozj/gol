package es.friki.gol.conversor;

import org.springframework.stereotype.Component;

import es.friki.gol.dto.EntornoDto;
import es.friki.gol.persistence.entity.Entorno;

@Component("entornoConv")
public class EntornoConvImpl implements EntornoConv {	

	@Override
	public EntornoDto toDto(Entorno model) {

		EntornoDto dto = new EntornoDto(model.getAncho(), model.getAlto());

		for (int x = 0; x < model.getAncho(); x++) {
			for (int y = 0; y < model.getAlto(); y++) {						
				dto.setCelula(x, y, model.getCelula(x, y).getEstado().getTexto());
			}
		}

		return dto;
	}

}
