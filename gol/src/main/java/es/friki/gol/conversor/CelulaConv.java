package es.friki.gol.conversor;

import es.friki.gol.dto.CelulaDto;
import es.friki.gol.persistence.entity.Celula;

public interface CelulaConv {
	
	public CelulaDto toDto(Celula model);

}
