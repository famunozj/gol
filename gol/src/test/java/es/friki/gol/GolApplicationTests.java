package es.friki.gol;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.friki.gol.controller.GolController;



@SpringBootTest
class GolApplicationTests {

	@Autowired
 	private GolController controller;

	@Test
	void contextLoads() {		
		assertNotEquals(null, controller);
	}
	

}
